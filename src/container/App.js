import React, { Component } from 'react';
import './App.css';
import Blocks from "../components/Blocks/Blocks";
import Tries from "../components/Tries/Tries";
import Button from "../components/Button/Button";

class App extends Component {

  state = {
    blocks: [],
    counter: 0,
    end: false,
  };

  componentDidMount = () => {
    this.drawNewBlocks();
  };

  endTheGame = () => {
    this.setState({end: true});
  };

  drawNewBlocks = () => {
    let array = [];
    let count = 36;
    while (count > 0) {
      array.push({ hasItem: false, isShown: false });
      count--;
    }
    let random = Math.floor(Math.random() * array.length);
    array[random].hasItem = true;
    this.setState({blocks: array, counter: 0, end: false});
  };

  isShown = (index) => {
    const checkIsShown = {...this.state.blocks[index]};
    if (checkIsShown.hasItem) {
      this.endTheGame();
    }
      checkIsShown.isShown = !checkIsShown.isShown;
      const blocks = [...this.state.blocks];
      blocks[index] = checkIsShown;
      this.setState(prevState => ({blocks, counter: prevState.counter + 1}));

  };

  render() {
    if (this.state.end) {
      alert(`You found the ring! you tries is ${this.state.counter}`);
    }
    return (
      <div className="App">
        <div className='field'>
          {this.state.blocks.map((item, index) => {
            return <Blocks checked={() => this.isShown(index)} show={item} key={index} />
          })}
        </div>
        <Tries counter={this.state.counter}/>
        <Button click={this.drawNewBlocks} />
      </div>
    );
  }
}

export default App;
