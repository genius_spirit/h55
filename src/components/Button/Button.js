import React from 'react';
import RaisedButton from 'material-ui/RaisedButton';

const Button = (props) => {
  return(
    <RaisedButton label='Reset' onClick={props.click} />
  )
};

export default Button;