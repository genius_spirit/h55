import React from 'react';
import './Blocks.css';

const Blocks = (props) => {
  let blockClasses = ['Blocks'];

  if (props.show.isShown) {
    blockClasses.push('Blocks-selected');
  }

  if (props.show.hasItem) {
    blockClasses.push('Blocks-showRing')
  }

  return <div className={blockClasses.join(' ')} onClick={props.checked}/>
};

export default Blocks;