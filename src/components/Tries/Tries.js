import React from 'react';
import './Tries.css'

const Tries = (props) => {
  return(
    <div className="TriesClass">Tries: {props.counter}</div>
  )
};

export default Tries;